package algorithms;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.awt.*;

public class MainTest {

    DefaultTeam d = new DefaultTeam();

    public ArrayList<Point> reader(String path, String fileName) throws FileNotFoundException {
        ArrayList<Point> read = new ArrayList<Point>();;
        Scanner obj = null;

        obj = new Scanner(new File(path, fileName));
        while (obj.hasNextInt()) {
            Point p = new Point(obj.nextInt(), obj.nextInt());
            read.add(p);
        }
        obj.close();
        return read;
    }
    
    public void testAlgoNaif() throws IOException {
        long elapsed = 0;
        long start = 0;
        long end = 0;

        StringBuilder sb = new StringBuilder();

        for (int i = 2; i <= 17; ++i) {
            ArrayList<Point> inputPoints = this.reader("samples", "test-" + Integer.toString(i) + ".points");

            start = System.currentTimeMillis();
            d.calculCercleMin(inputPoints);
            end = System.currentTimeMillis();
            elapsed = end - start;
            
            System.out.println(i);
            sb.append(i + " " + elapsed + "\n");

        }
        
        File file = new File("src/resultats", "resultsAlgoNaifSelonNbData.txt");
        BufferedWriter buf = new BufferedWriter(new FileWriter(file));
        buf.write(sb.toString());
        buf.close();
    }

    public static void main(String[] args) {
        MainTest test = new MainTest();
        try {
            test.testAlgoNaif();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}