package algorithms;

import java.awt.Point;
import java.util.ArrayList;

/***************************************************************
 * TME 1: calcul de diamètre et de cercle couvrant minimum.    *
 *   - Trouver deux points les plus éloignés d'un ensemble de  *
 *     points donné en entrée.                                 *
 *   - Couvrir l'ensemble de poitns donné en entrée par un     *
 *     cercle de rayon minimum.                                *
 *                                                             *
 * class Circle:                                               *
 *   - Circle(Point c, int r) constructs a new circle          *
 *     centered at c with radius r.                            *
 *   - Point getCenter() returns the center point.             *
 *   - int getRadius() returns the circle radius.              *
 *                                                             *
 * class Line:                                                 *
 *   - Line(Point p, Point q) constructs a new line            *
 *     starting at p ending at q.                              *
 *   - Point getP() returns one of the two end points.         *
 *   - Point getQ() returns the other end point.               *
 ***************************************************************/
import supportGUI.Circle;
import supportGUI.Line;

public class DefaultTeam {

	// calculDiametre: ArrayList<Point> --> Line
	// renvoie une paire de points de la liste, de distance maximum.
	public Line calculDiametre(ArrayList<Point> points) {
		if (points.size() < 2) {
			return null;
		}

		Point p = points.get(0);
		Point q = points.get(1);

		for (Point s : points)
			for (Point t : points)
				if (s.distance(t) > p.distance(q)) {
					p = s;
					q = t;
				}

		return new Line(p, q);
	}

	// calculCercleMin: ArrayList<Point> --> Circle
	// renvoie un cercle couvrant tout point de la liste, de rayon minimum.
	public Circle calculCercleMin(ArrayList<Point> points) {
		// return create_circle_from_points(points);
		 return welzlRec(points);
		// return welzIter(points);
		// return algoNaif(points);
	}

	private Circle algoNaif(ArrayList<Point> inputPoints) {
		ArrayList<Point> points = (ArrayList<Point>) inputPoints.clone();
		if (points.size() < 1)
			return null;
		double cX, cY, cRadius, cRadiusSquared;
		for (Point p : points) {
			for (Point q : points) {
				cX = .5 * (p.x + q.x);
				cY = .5 * (p.y + q.y);
				cRadiusSquared = 0.25 * ((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y));
				boolean allHit = true;
				for (Point s : points)
					if ((s.x - cX) * (s.x - cX) + (s.y - cY) * (s.y - cY) > cRadiusSquared) {
						allHit = false;
						break;
					}
				if (allHit)
					return new Circle(new Point((int) cX, (int) cY), (int) Math.sqrt(cRadiusSquared));
			}
		}
		double resX = 0;
		double resY = 0;
		double resRadiusSquared = Double.MAX_VALUE;
		for (int i = 0; i < points.size(); i++) {
			for (int j = i + 1; j < points.size(); j++) {
				for (int k = j + 1; k < points.size(); k++) {
					Point p = points.get(i);
					Point q = points.get(j);
					Point r = points.get(k);
					// si les trois sont colineaires on passe
					if ((q.x - p.x) * (r.y - p.y) - (q.y - p.y) * (r.x - p.x) == 0)
						continue;
					// si p et q sont sur la meme ligne, ou p et r sont sur la meme ligne, on les
					// echange
					if ((p.y == q.y) || (p.y == r.y)) {
						if (p.y == q.y) {
							p = points.get(k); // ici on est certain que p n'est sur la meme ligne de ni q ni r
							r = points.get(i); // parce que les trois points sont non-colineaires
						} else {
							p = points.get(j); // ici on est certain que p n'est sur la meme ligne de ni q ni r
							q = points.get(i); // parce que les trois points sont non-colineaires
						}
					}
					// on cherche les coordonnees du cercle circonscrit du triangle pqr
					// soit m=(p+q)/2 et n=(p+r)/2
					double mX = .5 * (p.x + q.x);
					double mY = .5 * (p.y + q.y);
					double nX = .5 * (p.x + r.x);
					double nY = .5 * (p.y + r.y);
					// soit y=alpha1*x+beta1 l'equation de la droite passant par m et
					// perpendiculaire a la droite (pq)
					// soit y=alpha2*x+beta2 l'equation de la droite passant par n et
					// perpendiculaire a la droite (pr)
					double alpha1 = (q.x - p.x) / (double) (p.y - q.y);
					double beta1 = mY - alpha1 * mX;
					double alpha2 = (r.x - p.x) / (double) (p.y - r.y);
					double beta2 = nY - alpha2 * nX;
					// le centre c du cercle est alors le point d'intersection des deux droites
					// ci-dessus
					cX = (beta2 - beta1) / (double) (alpha1 - alpha2);
					cY = alpha1 * cX + beta1;
					cRadiusSquared = (p.x - cX) * (p.x - cX) + (p.y - cY) * (p.y - cY);
					if (cRadiusSquared >= resRadiusSquared)
						continue;
					boolean allHit = true;
					for (Point s : points)
						if ((s.x - cX) * (s.x - cX) + (s.y - cY) * (s.y - cY) > Math.ceil(cRadiusSquared)) {
							allHit = false;
							break;
						}
					if (allHit) {
				//		System.out.println("Found r=" + Math.sqrt(cRadiusSquared));
						resX = cX;
						resY = cY;
						resRadiusSquared = cRadiusSquared;
					}
				}
			}
		}
		return new Circle(new Point((int) resX, (int) resY), (int) Math.sqrt(resRadiusSquared));
	}

	private Circle create_circle_from_points(ArrayList<Point> points) {
		int lg = points.size();

		switch (lg) {
		case 0:
			return null;
		case 1:
			return new Circle(points.get(0), 0);
		case 2:
			return new Circle(
					new Point((int) (points.get(0).x + points.get(1).x) / 2,
							(int) (points.get(0).y + points.get(1).y) / 2),
					(int) (points.get(0).distance(points.get(1)) / 2));
		case 3:
			int i = 0;
			int j = 1;
			int k = 2;

			Point p = points.get(i);
			Point q = points.get(j);
			Point r = points.get(k);
			// si les trois sont colineaires on passe
			if ((q.x - p.x) * (r.y - p.y) - (q.y - p.y) * (r.x - p.x) == 0)
				return new Circle(points.get(0), 0);
			// si p et q sont sur la meme ligne, ou p et r sont sur la meme ligne, on les
			// echange
			if ((p.y == q.y) || (p.y == r.y)) {
				if (p.y == q.y) {
					p = points.get(k); // ici on est certain que p n'est sur la meme ligne de ni q ni r
					r = points.get(i); // parce que les trois points sont non-colineaires
				} else {
					p = points.get(j); // ici on est certain que p n'est sur la meme ligne de ni q ni r
					q = points.get(i); // parce que les trois points sont non-colineaires
				}
			}
			// on cherche les coordonnees du cercle circonscrit du triangle pqr
			// soit m=(p+q)/2 et n=(p+r)/2
			double mX = .5 * (p.x + q.x);
			double mY = .5 * (p.y + q.y);
			double nX = .5 * (p.x + r.x);
			double nY = .5 * (p.y + r.y);
			// soit y=alpha1*x+beta1 l'equation de la droite passant par m et
			// perpendiculaire a la droite (pq)
			// soit y=alpha2*x+beta2 l'equation de la droite passant par n et
			// perpendiculaire a la droite (pr)
			double alpha1 = (q.x - p.x) / (double) (p.y - q.y);
			double beta1 = mY - alpha1 * mX;
			double alpha2 = (r.x - p.x) / (double) (p.y - r.y);
			double beta2 = nY - alpha2 * nX;

			// le centre c du cercle est alors le point d'intersection des deux droites
			// ci-dessus
			double cX = (beta2 - beta1) / (double) (alpha1 - alpha2);
			double cY = alpha1 * cX + beta1;

			// float cx = (float) ((p1.x + p2.x + p3.x)/3);
			// float cy = (float) ((p1.y + p2.y + p3.y)/3);

			Point center = new Point(Math.round((float) cX), Math.round((float) cY));
			double c_rayon = center.distance(p);
			return new Circle(center, Math.round((float) Math.ceil(c_rayon)));
		default:
			System.out.println("Erreur");
			return null;
		}
	}

	private Circle welzlRecAux(ArrayList<Point> p, ArrayList<Point> r) {
		Circle res;

		if (p.size() == 0 || r.size() == 3)
			return create_circle_from_points(r);

		Point p_pop = p.remove(0);
		res = welzlRecAux(p, r);

		if (res != null && res.getCenter().distance(p_pop) <= res.getRadius()) {
			p.add(p_pop);
			return res;
		}
		r.add(p_pop);
		res = welzlRecAux(p, r);
		r.remove(r.size() - 1);
		p.add(p_pop);

		return res;

	}

	private Circle welzlRec(ArrayList<Point> points) {
		return welzlRecAux(points, new ArrayList<Point>());
		// return create_circle_from_points(r);
	}
}
